
# Ultimate AI Backend Challenge  
  
Chat api that predicts your messages and sends response. 

API is running at, http://67.207.72.9:8080/chat endpoint.  
You can try it out with this request; 
```
curl --location --request POST 'http://67.207.72.9:8080/chat' \
--header 'Content-Type: application/json' \
--data-raw '{
    "botId": "5f74865056d7bb000fcd39ff",
    "message": "What commands do you have"
}'
```  

**Swagger UI;** http://67.207.72.9:8080/swagger-ui.html

**API Docs;** http://67.207.72.9:8080/v3/api-docs

**Seed database with these 5 intents;**
 - I want to speak with a human
 - Means or need to contact
 - What can I ask you  
 - Login problems
 - Greeting
  
**Technologies and Tools Used:**  
- Couchbase as database  
- Java 11  
- Spring Boot 2.5.2  
- Maven  
- Lombok   
- Devtools  
- OpenApi v3 with Swagger UI for documentation
  
**Deployment**  
- App is running on kubernetes cluster
- Used couchbase helm; https://github.com/couchbase-partners/helm-charts
- Docker Compose for local development  
- Own couchbase docker image for easy use on local environment (https://gitlab.com/gulteking/couchbase-server-sandbox)
- Gitlab CI for CI/CD tasks (see [pipeline](https://gitlab.com/gulteking/ultimateai-challenge/-/pipelines))  
  
## How to Run and Use  
Install `docker` and just run `docker-compose up` command inside root path of project. It ll set up couchbase database and run backend application from 8080 port.   
See **Requests** section for trying it out.  
  
## Development:  
You need Jdk11, Maven and docker, and couchbase db.   
To set-up couchbase, you can use compose file under `dev/couchbase/docker-compose.yml`  
### To compile;  
`mvn clean install`  
  
After successful build, you will find jar file in `target` directory.  
  
  
## Requests:  
  
**Chat Request**:  
```
curl --location --request POST 'http://67.207.72.9:8080/chat' \
--header 'Content-Type: application/json' \
--data-raw '{
    "botId": "5f74865056d7bb000fcd39ff",
    "message": "What commands do you have"
}'
```  
**Sample Response**:  
  ```
  {
"response": "You can ask anything you want.",
"intent": "What can I ask you?",
"confidence": 0.9685433506965637
}
  ```  
## Developers  
  
- Mehmet Fatih Gultekin (gulteking) - gultekin.mehmetfatih@gmail.com