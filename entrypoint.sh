#!/bin/sh
exec java $JAVA_OPTS \
-Djava.security.egd=file:/dev/./urandom \
-Dserver.port=$SERVER_PORT \
-Dcors.enabled.origins=$ENABLED_ORIGINS \
-Dcouchbase.url=$COUCHBASE_URL \
-Dcouchbase.username=$COUCHBASE_USERNAME \
-Dcouchbase.password=$COUCHBASE_PASSWORD \
-Dcouchbase.bucket=$COUCHBASE_BUCKET \
-Dresttemplate.timeout.seconds=$RESTTEMPLATE_TIMEOUT_SECONDS \
-Dai.api.key=$AI_API_KEY \
-Dai.request.url=$AI_API_BASE_URL \
-cp app:app/lib/* \
com.gulteking.ultimateai.challenge.ChallengeApplication