package com.gulteking.ultimateai.challenge.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gulteking.ultimateai.challenge.exception.InternalException;
import com.gulteking.ultimateai.challenge.model.ChatRequest;
import com.gulteking.ultimateai.challenge.model.ChatResponse;
import com.gulteking.ultimateai.challenge.service.ChatService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ChatControllerTest {

    @MockBean
    private ChatService chatService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void givenValidChatRequestWhenSendThenShouldReturnChatResponse() throws Exception {
        ChatResponse expectedResponse = ChatResponse.builder()
                .response("Hello, how can i help you")
                .intent("Greeting")
                .confidence(0.99D)
                .build();

        ChatRequest chatRequest = new ChatRequest("Hello, i want to talk with a human","botId");
        when(chatService.getReply(chatRequest)).thenReturn(expectedResponse);
        mockMvc.perform(post("/chat")
                .content(objectMapper.writeValueAsString(chatRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.response").value(expectedResponse.getResponse()))
                .andExpect(jsonPath("$.intent").value(expectedResponse.getIntent()))
                .andExpect(jsonPath("$.confidence").value(expectedResponse.getConfidence()));

    }


    @Test
    void givenInvalidChatRequestWhenSendThenShouldReturnBadRequest() throws Exception {

        ChatRequest chatRequest = new ChatRequest();
        mockMvc.perform(post("/chat")
                .content(objectMapper.writeValueAsString(chatRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    void givenValidChatRequestWhenErrorOccursThenShouldReturnInternalServerError() throws Exception {
        ChatRequest chatRequest = new ChatRequest("Hello, i want to talk with a human","botId");

        when(chatService.getReply(chatRequest)).thenThrow(new InternalException("Internal exception"));
        mockMvc.perform(post("/chat")
                .content(objectMapper.writeValueAsString(chatRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());

    }


}