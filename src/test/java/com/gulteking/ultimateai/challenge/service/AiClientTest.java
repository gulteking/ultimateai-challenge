package com.gulteking.ultimateai.challenge.service;


import com.gulteking.ultimateai.challenge.exception.ExceptionMessages;
import com.gulteking.ultimateai.challenge.exception.InternalException;
import com.gulteking.ultimateai.challenge.model.Prediction;
import com.gulteking.ultimateai.challenge.model.PredictionResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@SpringBootTest
public class AiClientTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private AiClient aiClient;


    @Test
    public void givenRequestWhenApiSuccessfulThenShouldReturnResponse() {
        PredictionResponse expectedResponseBody = new PredictionResponse();
        List<Prediction> expectedPredictions = new ArrayList<>();
        expectedResponseBody.setIntents(expectedPredictions);

        ResponseEntity<PredictionResponse> response = new ResponseEntity<>(expectedResponseBody, HttpStatus.OK);

        when(restTemplate.exchange(anyString(), eq(HttpMethod.POST), any(), eq(PredictionResponse.class))).thenReturn(response);

        List<Prediction> actualPredictions = aiClient.predict("botId","Hello");

        assertEquals(expectedPredictions, actualPredictions);
    }

    @Test
    public void givenRequestWhenApiReturnsErrorThenShouldThrowInternalException() {
        ResponseEntity<PredictionResponse> response = new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        when(restTemplate.exchange(anyString(), eq(HttpMethod.POST), any(), eq(PredictionResponse.class))).thenReturn(response);

        InternalException actualException = null;
        try {
            aiClient.predict("botId","Hello");
        } catch (InternalException ex) {
            actualException = ex;
        }
        assertNotNull(actualException);
        assertEquals(actualException.getMessage(), ExceptionMessages.AI_API_RETURNED_ERROR);
    }

    @Test
    public void givenRequestWhenApiReturnsEmptyBodyThenShouldThrowInternalException() {
        ResponseEntity<PredictionResponse> response = new ResponseEntity<>(null, HttpStatus.OK);
        when(restTemplate.exchange(anyString(), eq(HttpMethod.POST), any(), eq(PredictionResponse.class))).thenReturn(response);

        InternalException actualException = null;
        try {
            aiClient.predict("botId","Hello");
        } catch (InternalException ex) {
            actualException = ex;
        }
        assertNotNull(actualException);
        assertEquals(actualException.getMessage(), ExceptionMessages.AI_API_RETURNED_EMPTY_RESPONSE);

    }
}
