package com.gulteking.ultimateai.challenge.service;

import com.gulteking.ultimateai.challenge.entity.Intent;
import com.gulteking.ultimateai.challenge.model.ChatRequest;
import com.gulteking.ultimateai.challenge.model.ChatResponse;
import com.gulteking.ultimateai.challenge.model.Prediction;
import com.gulteking.ultimateai.challenge.repository.IntentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


@SpringBootTest
public class ChatServiceTest {

    @MockBean
    private PredictionService predictionService;

    @MockBean
    private IntentRepository intentRepository;

    @Autowired
    private ChatService chatService;

    @Value("${default.chat.response.message}")
    private String defaultResponse;

    @Test
    public void givenMessageWhenPredictionNotFoundThenShouldReturnDefaultReply() {
        ChatRequest chatRequest = new ChatRequest("AAAA", "botId");
        when(predictionService.predict(chatRequest.getBotId(), chatRequest.getMessage())).thenReturn(Optional.empty());

        ChatResponse chatResponse = chatService.getReply(chatRequest);

        assertNotNull(chatResponse);
        assertEquals(defaultResponse, chatResponse.getResponse());
        assertNull(chatResponse.getConfidence());
        assertNull(chatResponse.getIntent());
    }

    @Test
    public void givenMessageIntentWhenExistsInDatabaseThenShouldReturnReplyFromDb() {
        ChatRequest chatRequest = new ChatRequest("Hello there", "botId");
        Prediction prediction = new Prediction();
        prediction.setConfidence(0.99D);
        prediction.setName("Greeting");

        Intent intent = new Intent(UUID.randomUUID().toString(), prediction.getName(), "How can i help you?");
        when(predictionService.predict(chatRequest.getBotId(), chatRequest.getMessage())).thenReturn(Optional.of(prediction));
        when(intentRepository.findFirstByIntentName(prediction.getName())).thenReturn(Optional.of(intent));

        ChatResponse chatResponse = chatService.getReply(chatRequest);

        assertNotNull(chatResponse);
        assertEquals(intent.getReply(), chatResponse.getResponse());
        assertEquals(intent.getIntentName(), chatResponse.getIntent());
        assertEquals(prediction.getConfidence(), chatResponse.getConfidence());
    }


    @Test
    public void givenMessageIntentWhenNotExistsInDatabaseThenShouldReturnDefaultReply() {
        ChatRequest chatRequest = new ChatRequest("Hello there", "botId");
        Prediction prediction = new Prediction();
        prediction.setConfidence(0.99D);
        prediction.setName("Greeting");

        when(predictionService.predict(chatRequest.getBotId(), chatRequest.getMessage())).thenReturn(Optional.of(prediction));
        when(intentRepository.findFirstByIntentName(prediction.getName())).thenReturn(Optional.empty());

        ChatResponse chatResponse = chatService.getReply(chatRequest);

        assertNotNull(chatResponse);
        assertEquals(defaultResponse, chatResponse.getResponse());
        assertNull(chatResponse.getConfidence());
        assertNull(chatResponse.getIntent());

    }
}
