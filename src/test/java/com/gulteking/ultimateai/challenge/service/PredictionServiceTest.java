package com.gulteking.ultimateai.challenge.service;

import com.gulteking.ultimateai.challenge.model.Prediction;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class PredictionServiceTest {

    @MockBean
    private AiClient aiClient;

    @Autowired
    private PredictionService predictionService;

    @Test
    public void givenMessageWhenFindPredictionThenShouldReturnMostConfidentPrediction() {

        String botId = "botId";
        String message = "Hello";
        Prediction expectedPrediction = new Prediction(0.99, "Greeting");
        List<Prediction> predictions = new ArrayList<>();
        predictions.add(expectedPrediction);
        predictions.add(new Prediction(0.98, "Other"));
        predictions.add(new Prediction(0.97, "Need to talk with a human"));

        when(aiClient.predict(botId, message)).thenReturn(predictions);

        Optional<Prediction> actualPrediction = predictionService.predict(botId, message);

        assertTrue(actualPrediction.isPresent());
        assertEquals(expectedPrediction.getConfidence(), actualPrediction.get().getConfidence());
        assertEquals(expectedPrediction.getName(), actualPrediction.get().getName());
    }

    @Test
    public void givenMessageWhenFindPredictionNotFoundThenShouldReturnEmpty() {

        String botId = "botId";
        String message = "Hello";
        when(aiClient.predict(botId, message)).thenReturn(new ArrayList<>());

        Optional<Prediction> actualPrediction = predictionService.predict(botId, message);

        assertFalse(actualPrediction.isPresent());
    }
}