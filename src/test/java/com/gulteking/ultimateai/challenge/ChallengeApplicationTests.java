package com.gulteking.ultimateai.challenge;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ChallengeApplicationTests {

    @Test
    void contextLoads() {
        System.out.println("Context loaded");
    }

}
