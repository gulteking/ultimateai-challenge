package com.gulteking.ultimateai.challenge.controller;

import com.gulteking.ultimateai.challenge.model.ChatRequest;
import com.gulteking.ultimateai.challenge.model.ChatResponse;
import com.gulteking.ultimateai.challenge.service.ChatService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/chat")
@RequiredArgsConstructor
public class ChatController {

    private final ChatService chatService;

    @PostMapping
    public ChatResponse chatMessage(@RequestBody @Valid ChatRequest request) {
        return chatService.getReply(request);
    }
}
