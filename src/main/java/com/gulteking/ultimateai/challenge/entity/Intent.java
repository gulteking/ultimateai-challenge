package com.gulteking.ultimateai.challenge.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;

import javax.validation.constraints.NotNull;

@Document
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Intent {
    @Id
    private String id;

    @Field
    @NotNull
    private String intentName;

    @Field
    @NotNull
    private String reply;
}
