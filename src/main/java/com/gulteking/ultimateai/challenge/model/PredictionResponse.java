package com.gulteking.ultimateai.challenge.model;

import com.couchbase.client.core.deps.com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class PredictionResponse {
    private List<Prediction> intents;
}
