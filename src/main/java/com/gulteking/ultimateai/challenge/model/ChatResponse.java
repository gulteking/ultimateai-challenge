package com.gulteking.ultimateai.challenge.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ChatResponse {
    private String response;
    private String intent;
    private Double confidence;
}
