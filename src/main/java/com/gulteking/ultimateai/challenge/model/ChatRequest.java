package com.gulteking.ultimateai.challenge.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatRequest {

    @NotBlank
    @Size(max = 512)
    private String message;

    @NotBlank
    @Size(max = 255)
    private String botId;
}
