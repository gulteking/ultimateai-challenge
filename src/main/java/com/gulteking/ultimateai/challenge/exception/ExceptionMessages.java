package com.gulteking.ultimateai.challenge.exception;

public class ExceptionMessages {
    public static final String AI_API_RETURNED_ERROR = "AI_API_RETURNED_ERROR";
    public static final String AI_API_RETURNED_EMPTY_RESPONSE = "AI_API_RETURNED_EMPTY_RESPONSE";
}
