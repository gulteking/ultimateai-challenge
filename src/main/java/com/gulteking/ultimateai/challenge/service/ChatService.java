package com.gulteking.ultimateai.challenge.service;

import com.gulteking.ultimateai.challenge.entity.Intent;
import com.gulteking.ultimateai.challenge.model.ChatRequest;
import com.gulteking.ultimateai.challenge.model.ChatResponse;
import com.gulteking.ultimateai.challenge.model.Prediction;
import com.gulteking.ultimateai.challenge.repository.IntentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ChatService {

    private final PredictionService predictionService;
    private final IntentRepository intentRepository;

    @Value("${default.chat.response.message}")
    private String defaultResponse;


    public ChatResponse getReply(ChatRequest request) {
        Optional<Prediction> selectedPrediction = predictionService.predict(request.getBotId(),request.getMessage());
        if(selectedPrediction.isEmpty()){
            log.warn("Selected prediction is empty for message: {}, returning default response", request.getMessage());
            return ChatResponse.builder().response(defaultResponse).build();
        }

        Optional<Intent> intent =intentRepository.findFirstByIntentName(selectedPrediction.get().getName());
        if (intent.isEmpty()) {
            log.warn("Unable to find reply for selected prediction: {}, returning default response", selectedPrediction.get());
            return ChatResponse.builder().response(defaultResponse).build();
        }

        log.debug("Selected prediction: {} for message: {}, returning: {} as reply",
                selectedPrediction, request.getMessage(), intent.get().getReply());
        return ChatResponse.builder()
                .response(intent.get().getReply())
                .intent(selectedPrediction.get().getName())
                .confidence(selectedPrediction.get().getConfidence())
                .build();
    }


}
