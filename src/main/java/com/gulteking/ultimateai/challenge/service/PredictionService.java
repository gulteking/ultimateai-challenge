package com.gulteking.ultimateai.challenge.service;

import com.gulteking.ultimateai.challenge.model.ChatResponse;
import com.gulteking.ultimateai.challenge.model.Prediction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class PredictionService {

    private final AiClient aiClient;

    public Optional<Prediction> predict(String botId,String message) {
        List<Prediction> predictions = aiClient.predict(botId, message);
        return getMostConfidentPrediction(predictions);
    }


    private Optional<Prediction> getMostConfidentPrediction(List<Prediction> predictions) {
        return predictions.stream().max(Comparator.comparing(Prediction::getConfidence));
    }
}
