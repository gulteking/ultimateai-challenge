package com.gulteking.ultimateai.challenge.service;

import com.gulteking.ultimateai.challenge.exception.ExceptionMessages;
import com.gulteking.ultimateai.challenge.exception.InternalException;
import com.gulteking.ultimateai.challenge.model.Prediction;
import com.gulteking.ultimateai.challenge.model.PredictionRequest;
import com.gulteking.ultimateai.challenge.model.PredictionResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AiClient {

    private static final String AUTH_HEADER_KEY = "authorization";

    @Value("${ai.api.key}")
    private String apiKey;


    @Value("${ai.request.url}")
    private String requestUrl;

    private final RestTemplate restTemplate;

    public List<Prediction> predict(String botId, String message) {
        PredictionRequest requestBody = new PredictionRequest(botId, message);

        HttpEntity<PredictionRequest> requestEntity =
                new HttpEntity<>(requestBody, getHeaders());

        ResponseEntity<PredictionResponse> response =
                restTemplate.exchange(requestUrl, HttpMethod.POST, requestEntity,
                        PredictionResponse.class);

        if (!response.getStatusCode().is2xxSuccessful()) {
            log.error("ai api returned: {}", response.getStatusCode());
            throw new InternalException(ExceptionMessages.AI_API_RETURNED_ERROR);
        } else if (response.getBody() == null || response.getBody().getIntents() == null) {
            log.error("ai api response was empty");
            throw new InternalException(ExceptionMessages.AI_API_RETURNED_EMPTY_RESPONSE);
        }
        return response.getBody().getIntents();
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(AUTH_HEADER_KEY, apiKey);
        return headers;
    }
}
