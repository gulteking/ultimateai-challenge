package com.gulteking.ultimateai.challenge.repository;

import com.gulteking.ultimateai.challenge.entity.Intent;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface IntentRepository extends CrudRepository<Intent, String> {
    Optional<Intent> findFirstByIntentName(String intent);
}
